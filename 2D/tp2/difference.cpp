#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgLue2[250], cNomImgEcrite[250];
    int nH, nW, nTaille;

    if (argc != 4)
    {
        printf("Usage: Image1.pgm Image2.pgm ImageOut.pgm\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgLue2);
    sscanf(argv[3], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut, *Img2;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(Img2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue2, Img2, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            if(ImgIn[i*nW+j]!=Img2[i*nW+j]) {
                ImgOut[i*nW+j] = 0;
            } else {
                ImgOut[i*nW+j] = 255;
            }
        }
    }


    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}