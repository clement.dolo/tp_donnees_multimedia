> **test_grey.cpp:**
>     ```./test_grey <ImgIn.pgm> <ImgOut.pgm> <Seuil>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>           <li> Seuil : Int : Compris entre 0 et 255 </li>
>       </ul>

>**erosion.cpp:**
>    ```./erosion <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **dilatation.cpp:** 
>    ```./dilatation <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **fermeture.cpp:**
>    ```./fermeture <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **ouverture.cpp:**
>    ```./ouverture <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **difference.cpp:**
>    ```./difference <Img1.pgm> <Img2.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> Img1.pgm : String : Nom du permier fichier original </li>
>           <li> Img2.pgm : String : Nom du deuxieme fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>
