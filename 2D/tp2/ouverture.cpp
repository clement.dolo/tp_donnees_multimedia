#include <stdio.h>
#include "image_ppm.h"

bool LightPixelInNeighborhood(OCTET *image, int x, int y, int nW)
{
    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            if (image[(x + i) * nW + (y + j)] == 255)
            {
                return true;
            }
        }
    }
    return false;
}

bool DarkPixelInNeighborhood(OCTET *image, int x, int y, int nW)
{
    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            if (image[(x + i) * nW + (y + j)] == 0)
            {
                return true;
            }
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;

    if (argc != 3)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut, *ImgTemp;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgTemp, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    // Erosion
    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            if (DarkPixelInNeighborhood(ImgIn, i, j, nW) == true)
            {
                ImgTemp[i * nW + j] = 0;
            }
            else
            {
                ImgTemp[i * nW + j] = 255;
            }
        }
    }
    // Dilatation
    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            if (LightPixelInNeighborhood(ImgTemp, i, j, nW) == true)
            {
                ImgOut[i * nW + j] = 255;
            }
            else
            {
                ImgOut[i * nW + j] = 0;
            }
        }
    }


    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}