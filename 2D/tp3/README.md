> **inverse.cpp:**
>     ```./inverse <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

>**filtre_flou1.cpp:**
>    ```./filtre_flou1 <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **filtre_flou2.cpp:** 
>    ```./filtre_flou2 <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **flou_ppm.cpp:**
>    ```./flou_ppm <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **filtre_flou.cpp:**
>    ```./filtre_flou <ImgIn.pgm> <ImgOut.pgm> <TauxFloutage>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
            <li> TauxFloutage : Int : Taux de floutage </li>
>       </ul>
