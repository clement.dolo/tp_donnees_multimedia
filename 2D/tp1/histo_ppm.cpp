#include <stdio.h>
#include "image_ppm.h"
#include <fstream>

int main(int argc, char const *argv[])
{
    
    char imgFile[250], histoFile[250];
    int nCL, type;

    sscanf(argv[1],"%s", imgFile);
    sscanf(argv[2],"%s", histoFile);

    OCTET *ImgIn;
    int nH, nW;
    lire_nb_lignes_colonnes_image_pgm(imgFile, &nH, &nW);
  
    allocation_tableau(ImgIn, OCTET, nH * nW);
    lire_image_pgm(imgFile, ImgIn, nH * nW);

    std::ofstream file;
    file.open(histoFile);

    int rCounts[256] = {0};
    int gCounts[256] = {0};
    int bCounts[256] = {0};
    for (int i = 0; i < nH*nW; i+=3)
    {
        rCounts[ImgIn[i]]++;
        gCounts[ImgIn[i+1]]++;
        bCounts[ImgIn[i+2]]++;

    }
    for (int i = 0; i < 256; i++)
    {
        file<<i<<" "<<rCounts[i]<<" "<<gCounts[i]<<" "<<bCounts[i]<<std::endl;
    }
    
    return 0;
}
