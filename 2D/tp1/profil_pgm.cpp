#include <stdio.h>
#include "image_ppm.h"
#include <fstream>

int main(int argc, char const *argv[])
{
    
    char imgFile[250], histoFile[250];
    int nCL, type;

    sscanf(argv[1],"%s", imgFile);
    sscanf(argv[2],"%s", histoFile);
    sscanf(argv[3],"%d", &type);
    sscanf(argv[4],"%d", &nCL);

    OCTET *ImgIn;
    int nH, nW, nTaille;
    lire_nb_lignes_colonnes_image_pgm(imgFile, &nH, &nW);
    nTaille = nH * nW;
  
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(imgFile, ImgIn, nH * nW);

    std::ofstream file;
    file.open(histoFile);

    if(type == 0){
        printf("Column %d\n", nCL);
        for (int i = 0; i < nH; i++)
        {
            file<<i<<" "<<int(ImgIn[i*nW+nCL])<<std::endl;
        }
    }
    else if(type == 1) {
        printf("Line %d\n", nCL);
        for (int i = 0; i < nW; i++)
        {
            file<<i<<" "<<int(ImgIn[nCL*nW+i])<<std::endl;
        }
    }
    else {
        printf("choose:\n - 0 for column\n - 1 for line");
        exit(1);
    }

    
    return 0;
}
