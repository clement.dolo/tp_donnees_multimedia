// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int S[argc - 3];

  if (argc < 4)
  {
    printf("Usage: ImageIn.pgm ImageOut.pgm [Seuil1]*\n");
    exit(1);
  }

  sscanf(argv[1], "%s", cNomImgLue);
  sscanf(argv[2], "%s", cNomImgEcrite);
  for (int i = 3; i < argc; i++)
  {
    sscanf(argv[i], "%d", &S[i - 3]);
  }

  OCTET *ImgIn, *ImgOut;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;

  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille);

  // Parcours de l'image pixel par pixel
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
    {
      // Parcours des seuils à la recherche du plus petit "plus
      // grand que niveau de gris"
      for (int k = 0; k < argc - 3; k++)
      {
        if (ImgIn[i * nW + j] < S[k])
        {
          ImgOut[i * nW + j] = S[k];
          break;
        }
        ImgOut[i * nW + j] = 255;
      }
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
  free(ImgIn);
  free(ImgOut);

  return 1;
}
