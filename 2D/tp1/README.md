> **seuillage_pgm.cpp:**
>     ```./seuillage_pgm <ImgIn.pgm> <ImgOut.pgm> <Seuil> <SeuilN>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier après seuillage </li>
>           <li> Seuil : Int : Compris entre 0 et 255 </li>
>           <li> *SeuilN : Int : Autant de Seuil que souhaités. (Optionnel)* </li>
>       </ul>

>**seuillage_ppm.cpp:**
>    ```./seuillage_ppm <ImgIn.pgm> <ImgOut.pgm> <Seuil>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier après seuillage </li>
>           <li> Seuil : Int : Compris entre 0 et 255 </li>
>       </ul>

> **seuillage_rgb_ppm.cpp:** 
>    ```./seuillage_rgb_ppm <ImgIn.pgm> <ImgOut.pgm> <SeuilR> <SeuilG> <SeuilB>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier après seuillage </li>
>           <li> SeuilR : Int : Compris entre 0 et 255, seuil de la composante Rouge </li>
>           <li> SeuilG : Int : Compris entre 0 et 255, seuil de la composante Verte </li>
>           <li> SeuilB : Int : Compris entre 0 et 255, seuil de la composante Bleue </li>
>       </ul>

> **histo_pgm.cpp:**
>    ```./histo_pgm <ImgIn.pgm> <File.dat>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> File.dat : String : Nom du fichier de sortit pour les données </li>
>       </ul>

> **histo_ppm.cpp:**
>    ```./histo_ppm <ImgIn.pgm> <File.dat>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> File.dat : String : Nom du fichier de sortit pour les données </li>
>       </ul>

> **profil_pgm.cpp:**
>    ```./profil_pgm <ImgIn.pgm> <File.dat> <Mode> <Index>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> File.dat : String : Nom du fichier de sortit pour les données </li>
>           <li> Mode : Boolean : 0 = Colonne, 1 = Ligne </li>
>           <li> Index : Int : Numéro de la colonne/ligne </li>
>       </ul>

> **optimal_pgm.cpp:**
>    ```./optimal_pgm <ImgIn.pgm> <NombreSeuils>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> NombreSeuils : Int : Nombre de seuils voulu </li>
>       </ul>
