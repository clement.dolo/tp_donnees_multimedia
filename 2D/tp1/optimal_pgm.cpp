#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char pgmFile[250];
    int nH, nW, nTaille, S;

    if (argc != 3)
    {
        printf("Usage: Image.pgm NombreSeuil\n Exemple: 2_Lena.pgm 5\n");
        exit(1);
    }

    sscanf(argv[1], "%s", pgmFile);
    sscanf(argv[2], "%d", &S);

    OCTET *ImgIn;

    lire_nb_lignes_colonnes_image_pgm(pgmFile, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(pgmFile, ImgIn, nH * nW);

    int count[256] = {0};
    for (int i = 0; i < nTaille; i++)
    {
        count[ImgIn[i]]++;
    }
 
    //Parcours des comptes par secteurs
    for (int i = 0; i < S; i++)
    {
        int max[2] = {0};
        for (int j = 0; j < 256/S+1; j++)
        {
            int iter = i*(256/S+1)+j < 255 ? i*(256/S+1)+j : 255;
            //Recherche de la valeur la plus fréquente
            if (max[1] < count[iter]) {
                max[0] = iter;
                max[1] = count[iter]; 
            }
        }
        printf("%d ",max[0]);
    }

    printf("\n");

    free(ImgIn);

    return 1;
}
