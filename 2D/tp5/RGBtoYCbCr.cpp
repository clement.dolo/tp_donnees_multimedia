#include <stdio.h>
#include "../tp2/image_ppm.h"
#include <fstream>

int minmax(int min, int max, double x) {
    if (x < min)
    {
        return min;
    } else if (x>max)
    {
        return max;
    }
    return (int) x;
}

int main(int argc, char const *argv[])
{

    char imgFile[250], outFile[250];

    sscanf(argv[1], "%s", imgFile);
    sscanf(argv[2], "%s", outFile);

    OCTET *ImgIn, *outY, *outCb, *outCr;
    int nH, nW;
    lire_nb_lignes_colonnes_image_ppm(imgFile, &nH, &nW);

    allocation_tableau(ImgIn, OCTET, nH * nW * 3);
    lire_image_ppm(imgFile, ImgIn, nH * nW);
    allocation_tableau(outY, OCTET, nH * nW);
    allocation_tableau(outCb, OCTET, nH * nW);
    allocation_tableau(outCr, OCTET, nH * nW);

    for (int i = 0; i < nH * nW; i++)
    {
        outY[i] = minmax(0, 255, ImgIn[i * 3] * 0.299 + ImgIn[i * 3 + 1] * 0.587 + ImgIn[i * 3 + 2] * 0.114);
        outCb[i] = minmax(0, 255, ImgIn[i * 3] * -0.1687 + ImgIn[i * 3 + 1] * -0.3313 + ImgIn[i * 3 + 2] * 0.5 + 128);
        outCr[i] = minmax(0, 255, ImgIn[i * 3] * 0.5 + ImgIn[i * 3 + 1] * -0.4187 + ImgIn[i * 3 + 2] * -0.0813 + 128);
    }

    ecrire_image_pgm(strcat(outFile, "_Y.pgm"), outY, nH, nW);
    sscanf(argv[2], "%s", outFile);
    ecrire_image_pgm(strcat(outFile, "_Cb.pgm"), outCb, nH, nW);
    sscanf(argv[2], "%s", outFile);
    ecrire_image_pgm(strcat(outFile, "_Cr.pgm"), outCr, nH, nW);
    free(ImgIn);
    free(outY);
    return 0;
}