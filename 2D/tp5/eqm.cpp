#include <stdio.h>
#include "../tp2/image_ppm.h"
#include <fstream>

int main(int argc, char const *argv[])
{

    char firstFile[250], secFile[250];

    sscanf(argv[1], "%s", firstFile);
    sscanf(argv[2], "%s", secFile);

    OCTET *Img1, *Img2;
    int nH, nW;
    lire_nb_lignes_colonnes_image_pgm(firstFile, &nH, &nW);

    allocation_tableau(Img1, OCTET, nH * nW);
    lire_image_pgm(firstFile, Img1, nH * nW);
    allocation_tableau(Img2, OCTET, nH * nW);
    lire_image_pgm(secFile, Img2, nH * nW);

    int sum = 0;
    for (int i = 0; i < nH * nW; i++)
    {
        sum += pow(Img1[i] - Img2[i], 2);
    }
    printf("EQM: %d\n", sum / (nH * nW));

    free(Img1);
    free(Img2);
    return 0;
}
