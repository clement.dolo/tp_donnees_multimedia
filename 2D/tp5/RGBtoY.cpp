#include <stdio.h>
#include "../tp2/image_ppm.h"
#include <fstream>

int main(int argc, char const *argv[])
{
    
    char imgFile[250], outFile[250];

    sscanf(argv[1],"%s", imgFile);
    sscanf(argv[2],"%s", outFile);

    OCTET *ImgIn, *ImgOut;
    int nH, nW;
    lire_nb_lignes_colonnes_image_ppm(imgFile, &nH, &nW);
  
    allocation_tableau(ImgIn, OCTET, nH * nW*3);
    lire_image_ppm(imgFile, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nH * nW);

    printf("%d %d", nH, nW);
    for (int i = 0; i < nH*nW; i++)
    {
        ImgOut[i] = (ImgIn[i*3]+ImgIn[i*3+1]+ImgIn[i*3+2])/3;
    }

    ecrire_image_pgm(outFile, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);
    return 0;
}
