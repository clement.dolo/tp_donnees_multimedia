#include <stdio.h>
#include "../tp2/image_ppm.h"
#include <fstream>

int minmax(int min, int max, double x)
{
    if (x < min)
    {
        return min;
    }
    else if (x > max)
    {
        return max;
    }
    return (int)x;
}

int main(int argc, char const *argv[])
{

    char inFile[250], outFile[250];
    int k;
    sscanf(argv[1], "%s", inFile);
    sscanf(argv[2], "%s", outFile);
    sscanf(argv[3], "%d", &k);

    OCTET *arrY, *arrOut;
    int nH, nW;
    lire_nb_lignes_colonnes_image_pgm(inFile, &nH, &nW);

    allocation_tableau(arrY, OCTET, nH * nW);
    lire_image_pgm(inFile, arrY, nH * nW);
    allocation_tableau(arrOut, OCTET, nH * nW);

    for (int i = 0; i < nH * nW; i++)
    {
        arrOut[i] = minmax(0, 255, arrY[i] + k);
    }

    ecrire_image_pgm(outFile, arrOut, nH, nW);
    free(arrY);
    free(arrOut);
    return 0;
}
