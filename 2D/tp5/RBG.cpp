#include <stdio.h>
#include "../tp2/image_ppm.h"
#include <fstream>

int minmax(int min, int max, double x) {
    if (x < min)
    {
        return min;
    } else if (x>max)
    {
        return max;
    }
    return (int) x;
}

int main(int argc, char const *argv[])
{

    char inY[250], outFile[250], inCb[250], inCr[250];

    sscanf(argv[1], "%s", inY);
    sscanf(argv[2], "%s", inCb);
    sscanf(argv[3], "%s", inCr);
    sscanf(argv[4], "%s", outFile);

    OCTET *arrY, *arrCb, *arrCr, *arrOut;
    int nH, nW;
    lire_nb_lignes_colonnes_image_pgm(inY, &nH, &nW);

    allocation_tableau(arrY, OCTET, nH * nW);
    lire_image_pgm(inY, arrY, nH * nW);
    allocation_tableau(arrCb, OCTET, nH * nW);
    lire_image_pgm(inCb, arrCb, nH * nW);
    allocation_tableau(arrCr, OCTET, nH * nW);
    lire_image_pgm(inCr, arrCr, nH * nW);
    allocation_tableau(arrOut, OCTET, nH * nW * 3);

    for (int i = 0; i < nH * nW; i++)
    {
        arrOut[i * 3] = minmax(0, 255, arrY[i] + 1.402 * (arrCr[i] - 128));
        arrOut[i * 3 + 1] = minmax(0, 255, arrY[i] + 1.722 * (arrCb[i] - 128));
        arrOut[i * 3 + 2] = minmax(0, 255, arrY[i] - 0.34414 * (arrCb[i] - 128) - 0.71414 * (arrCr[i] - 128));
    }

    ecrire_image_ppm(outFile, arrOut, nH, nW);
    free(arrY);
    free(arrCb);
    free(arrCr);
    free(arrOut);
    return 0;
}
