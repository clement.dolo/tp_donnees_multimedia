> **norme_gradient.cpp:**
>     ```./norme_gradient <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

>**seuil.cpp:**
>    ```./seuil <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

> **seuil_hysteresis.cpp:** 
>    ```./seuil_hysteresis <ImgIn.pgm> <ImgOut.pgm> SeuilBas SeuilHaut```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>           <li> SeuilBas : Int : Seuil bas </li>
>           <li> SeuilHaut : Int : Seuil haut </li>
>       </ul>

> **gaussian_blur.cpp:**
>    ```./gaussian_blur <ImgIn.pgm> <ImgOut.pgm>```
>       <ul>
>           <li> ImgIn.pgm : String : Nom du fichier original </li>
>           <li> ImgOut.pgm : String : Nom du fichier de sortie </li>
>       </ul>

