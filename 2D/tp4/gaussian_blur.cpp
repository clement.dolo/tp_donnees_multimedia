#include <stdio.h>
#include "../tp2/image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, SD;

    if (argc != 4)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm StandardDeviation\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &SD);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    double K[5][5];
    for (int i = -2; i < 3; i++)
        for (int j = -2; j < 3; j++)
        {
            double DPISD = 2 * pow(SD, 2);
            double E = exp(-1 * ((pow(i, 2) + pow(j, 2))) / DPISD);
            K[i+2][j+2] = E / (M_PI * DPISD);
        }

    for (int i = 0; i < nH; i++)
        for (int j = 0; j < nW; j++)
        {
            int sum = 0;
            int count = 1;
            for (int k = -2; k <= 2; k++)
                for (int l = -2; l <= 2; l++)
                {
                    if (i+k>=0 && i+k<nH && j+l>=0 && j+l<nW)
                    {
                        sum += ImgIn[(i + k) * nW + (j + l)]*K[k+2][l+2];
                    }
                }
            ImgOut[i * nW + j] = (int) sum;
        }
        
    

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}