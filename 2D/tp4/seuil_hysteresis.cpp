#include <stdio.h>
#include "../tp2/image_ppm.h"

bool LightPixelInNeighborhood(OCTET *image, int x, int y, int nW)
{
    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            if (image[(x + i) * nW + (y + j)] == 0)
            {
                return true;
            }
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, SB, SH;

    if (argc != 5)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm SeuilBas SeuilHaut\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &SB);
    sscanf(argv[4], "%d", &SH);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i < nH; i++)
        for (int j = 0; j < nW; j++)
        {
            int pix = ImgIn[i * nW + j];
            if (pix <= SB)
            {
                ImgOut[i * nW + j] = 0;
            }
            else if (pix >= SH)
            {
                ImgOut[i * nW + j] = 255;
            }
        }

    for (int i = 0; i < nTaille; i++)
    {
        ImgIn[i] = ImgOut[i];
    }

    for (int i = 0; i < nH; i++)
        for (int j = 0; j < nW; j++)
        {
            int pix = ImgIn[i * nW + j];
            if (pix > SB && pix < SH)
            {
                if (LightPixelInNeighborhood(ImgIn, i, j, nW) == true)
                {
                    ImgOut[i * nW + j] = 255;
                }
                else
                {
                    ImgOut[i * nW + j] = 0;
                }
            }
        }

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}