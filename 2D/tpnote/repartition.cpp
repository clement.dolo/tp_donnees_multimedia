#include <stdio.h>
#include "image_ppm.h"
#include <fstream>

int main(int argc, char const *argv[])
{
    
    char imgFile[250], histoFile[250];
    sscanf(argv[1],"%s", imgFile);
    sscanf(argv[2],"%s", histoFile);

    OCTET *ImgIn;
    int nH, nW, nTaille;
    lire_nb_lignes_colonnes_image_pgm(imgFile, &nH, &nW);
    nTaille = nH * nW;
  
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(imgFile, ImgIn, nH * nW);

    std::ofstream file;
    file.open(histoFile);
    double counts[256] = {0};

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            counts[ImgIn[i*nW+j]]++;
        }
    }

    double ddp[256];
    for (int i = 0; i < 256; i++)
    {
        ddp[i] = counts[i]/nTaille;
    }

    double rep[256];
    rep[0] = ddp[0];
    for (int i = 1; i < 256; i++)
    {
        rep[i] = rep[i-1] + ddp[i];
    }

    for (int i = 0; i < 256; i++)
    {
        file<<i<<" "<<rep[i]<<std::endl;
    }

    return 0;
}
