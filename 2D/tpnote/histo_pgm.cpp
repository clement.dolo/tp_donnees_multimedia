#include <stdio.h>
#include "image_ppm.h"
#include <fstream>

int main(int argc, char const *argv[])
{
    
    char imgFile[250], histoFile[250];
    sscanf(argv[1],"%s", imgFile);
    sscanf(argv[2],"%s", histoFile);

    OCTET *ImgIn;
    int nH, nW, nTaille;
    lire_nb_lignes_colonnes_image_pgm(imgFile, &nH, &nW);
    nTaille = nH * nW;
  
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(imgFile, ImgIn, nH * nW);

    std::ofstream file;
    file.open(histoFile);
    int counts[256] = {0};

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            counts[ImgIn[i*nW+j]]++;
        }
    }

    for (int i = 0; i < 256; i++)
    {
        file<<i<<" "<<counts[i]<<std::endl;
    }

    return 0;
}
